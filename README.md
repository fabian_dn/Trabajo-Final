## MANUAL DE MIGRACIÓN A SOFTWARE LIBRE PARA REDES DE OFICINA


**Autor:** Delgado, Carlos Fabián

**Dirección** Prof. Bayo, Martín



**Introducción:**

Desde mi ámbito laboral, como comerciante y asesor del área informática he tenido la oportunidad de constatar, in situ, la problemática a la que se enfrentan mis clientes
en sus diferentes comercios y actividades de negocios al trabajar con software propietario, quedando sujetos a un determinado proveedor, en la mayoría de las ocasiones
violando la ley al no cumplir con las condiciones establecidas en las licencias o deliberadamente utilizando software sin adquirir una licencia,  recurriendo a la
instalación de programas de dudosa procedencia, que en la mayoría de los casos instalan software malicioso o habilitan puertas traseras que pueden ser utilizadas para 
diferentes tipos de delitos informáticos, y así obtener licencias de forma ilegal, poniendo en riesgo a toda la empresa.

Este tipo de situaciones con las que a diario me encontraba y me sigo encontrando, me llevaron a buscar una solución, la cual vino de la mano del Software Libre y la 
Universidad Nacional del Litoral.

Con la problemática antes mencionada latente y los conocimientos adquiridos en la Tecnicatura Universitaria en Software Libre, es que realizare como Trabajo Final de la 
carrera, un MANUAL DE MIGRACIÓN A SOFTWARE LIBRE PARA REDES DE OFICINA. 


**Desarrollo:**

En el presente manual sera abordado el trabajo de migración de la red de computadoras de la clínica de consultorios médicos, Centro Profesional Congreso, dicha clínica 
cuenta con 10 estaciones de trabajo, de las cuales una de las estaciones de trabajo es la que tiene conectada una impresora y un espacio compartido en disco con archivos
comunes a todos los usuarios, esta impresora y espacio de disco son compartidos con todas las terminales de la red, en las cuales se instalara, en cada una de las 
estaciones de trabajo un Sistema Operativo GNU/Linux, con las aplicaciones requeridas para el trabajo diario, Navegador de Internet, Cliente de Correo Electrónico,
Mensajero Instantáneo, Suite de Oficina y Cliente de escritorio para administración de espacio compartido en disco. Ademas sera instalado en la computadora que dará 
servicio de impresión y nube de datos, el software para compartir la impresora y el software del servidor de archivos.

En un principio también se tuvieron en cuenta los formatos de archivos para la realización de la migración. Uno de los grandes problemas que presentan las migraciones
a software libre es la migración de datos. Muchas de las aplicaciones basadas en software libre disponen de compatibilidad con sus equivalentes privativos y los formatos
usados por estos y/u opciones para importar esta información a formatos libres. No obstante, algunas funciones o características pueden no funcionar correctamente debido
al uso de dichos formatos privativos.

En el Centro Profesional Congreso se utilizan archivos en formatos privativos por lo tanto se planteo la necesidad de migrar a formatos libres, pero por decisión de la
empresa se opto por continuar trabajando con los formatos privativos. 


**Estado del Arte:**

Se a indagado en Internet sobre migración a sistemas operativos y aplicaciones libres en el ámbito laboral y si bien se han encontrado varios trabajos y bibliografías
la mayoría son meramente teóricos y solo contienen métodos de migración y por otro lado, trabajos con definiciones de que son cada elemento a tener en cuenta, pero
llevados a la practica, no figura en los mismos  de que manera hacerlo, por ejemplo, definir usuarios, permisos o sistemas de archivos, por tener en cuenta algunos de los
muchos conceptos que deben aplicarse, como tampoco que aplicaciones de software utilizar.

Estos son algunos ejemplos de este tipo de trabajos y bibliografías:
* MIGRACIÓN ESCRITORIO SOFTWARE LIBRE, Eduardo Romero Moreno, Ayuntamiento de Zaragoza.
  El objetivo de este documento es ofrecer una visión global de la información y procesos que se necesitan conocer para poder planificar y realizar una migración con éxito
  de los entornos de escritorio a herramientas de software libre. Este documento es una guía de buenas prácticas, no manual. En el mismo se ofrece una Guía Metodológica
  con una visión acerca de los pasos y procesos necesarios para migrar los escritorios de una organización a herramientas de software libre. Se enumeran y describen cuales
  son los principales elementos necesarios para que esta se produzca con éxito. Es una guía neutral sin referencias tecnológicas concretas. También se ofrece una Guía
  Técnica con puntos de vista apropiados para realizar este proceso.
* EL SOFTWARE LIBRE Y SU DIFUSIÓN EN ARGENTINA: MERCADO, ESTADO, SOCIEDAD, Agustin Zanotti, Universidad Nacional de Villa Maria.
  El artículo analiza la difusión del software libre en Argentina, a partir de un recorrido exploratorio sobre diferentes campos. Señala en este sentido algunas
  experiencias en curso y abre la discusión sobre sus posibilidades y límites en la región. El estudio se basa en entrevistas, observaciones de campo y análisis de sitios
  web realizados entre 2010 y 2013 .
* MIGRACIÓN A SOFTWARE LIBRE. GUÍA DE BUENAS PRACTICAS, SOFTWARE LIBRE EN LA EMPRESA, Instituto Tecnológico de Informática, Generalitat Valenciana, IMPIVA, Union Europea,
  Daniel Saez, Martín Peris, Ricard Roca, David Anes.
  Este articulo es una completa guía teórica con el detalle de los pasos a seguir, desde la motivación para realizar la migración hasta la implementación final de una
  migración a Software Libre en el ámbito empresarial.
* MIGRACIÓN DE FLEMING Y MARTOLIO SRL A SOFTWARE LIBRE, Gustavo A. Courault, Carlos Gutierrez.
  Fleming y Martolio SRL es una empresa distribuidora de neumáticos en la zona Noreste de la Argentina con 11 sucursales y una amplia red de distribuidores. La firma
  comenzó a informatizarse en el año 1995, con un sistema basado en MS-DOS. La migración se realizo en 3 etapas, en la primer etapa se realizo la migración de la casa
  central, en una segunda etapa se migraron las sucursales y en la tercera etapa se migro el aplicativo de gestión comercial.

En este proyecto final, lo que se pretende realizar es un trabajo, tanto Teórico, como así también el detalle  Practico, de como se realiza la migración de una red de
computadoras y que aplicativos de software utilizar, en un entorno basado en Software Libre. Por lo tanto si bien se podrían consultar los trabajos antes mencionados para
ver por ejemplo la estructura y orden llevado a cabo, se hará énfasis en el desarrollo de la parte practica, misma que fue dictada en las materias de Administración 
GNU/Linux, Software Libre Ofimática en las Organizaciones, Tecnologías Web, Redes y Bases de Datos, de la carrera.

El Software que se utilizara en la realización de esta migración, en primera instancia fue elegido por poseer alguna de las licencias de Software Libre existentes, pero
también y no en menor medida, fueron seleccionados  por la transparencia de los mismos, respecto de su contraparte privativa.




**Objetivos**

**Generales:**
    
Como objetivo general, con este trabajo, lo que se pretende es la migración
final completa a Software Libre de una red de computadoras, en principio de 
una clínica de consultorios médicos, pero también se pretende lograr un 
trabajo que pueda ser llevado a cabo en diferentes tipos de oficinas donde 
se trabaje con redes de computadoras.

**Específicos:**

  Los objetivos específicos del presente trabajo son los siguientes:
- Ampliar la funcionalidad del sistema, con la migración a software libre se conseguirá dotar a cada uno de los usuarios de la red de un espacio de uso particular al que
  solo accederá mediante su usuario y contraseña.  Además se instalara una  impresora en red que sera compartida por todas las terminales y un espacio compartido en un
  servidor de archivos. 
- Mejorar la productividad del sistema, ya que funcionara de forma correcta, eficiente y no se vera degradado en el tiempo debido a la menor incidencia de virus, spyware
  y malware. Además, llegado el momento de adquisición y puesta en marcha de nuevos equipos, se ahorrara tiempo de puesta en marcha, al solo tener que clonar discos, 
  configurar el equipo en red y definir el usuario.
- Regularización de la empresa respecto a las licencias de software, si bien se cuenta con dos maquinas que poseen la licencia del sistema operativo, no poseen licencia
  del paquete de oficina, las demás computadoras no poseen licencia alguna.
- Reducción de costos, se lograra de distintas maneras, por ejemplo en la adquisición de licencias, mantenimiento de equipos informáticos, reutilización de hardware, etc.
- Hacer que la migración a Software Libre sea lo mas transparente y fácil de asimilar posible, para todo el personal de la empresa. 

**Metodología:**
    
Para llevar a cabo la migración de escritorios es crucial establecer un conjunto de procedimientos para llegar a un objetivo claro. Establecer la metodología para la
migración de nuestros sistemas habitualmente requerirá de una importante cantidad de recursos humanos tanto en tiempo como en mano de obra.
Como primera actividad se realizara un relevamiento de hardware, software y forma de trabajo del personal, a fin de establecer la factibilidad de reutilización de hardware
existente, se seleccionaran programas que sean Software Libre para sustituir el Software Privativo en uso y se investigara la metodología de trabajo, con el objetivo de
minimizar el impacto de la migración, en el trabajo diario del personal. 
Aprovechando las capacidades de migración y clonado de sistemas informáticos completos entre diferentes computadoras (modelos y marcas de dispositivos, tales como placas 
madre, procesadores, memorias, etc), que poseen las distribuciones de GNU/Linux, primeramente se realizara la instalación y configuración completa de la computadora 
servidor y ademas también de una computadora cliente, en maquinas que no pertenecen a la red de trabajo de la empresa, de esta manera se lograra minimizar el downtime
llegado el momento de la migración, ya que una vez que se cuente con las imágenes de los sistemas instalados, solo restara clonar los discos, cambiar las configuraciones
de red, crear los usuarios de sistema y mensajería instantánea correspondientes. 
En el momento en que las maquinas de pruebas se encuentren 100 por ciento operativas, se realizara una primera evaluación de los resultados obtenidos, junto a directivos
y empleados de la empresa, en la misma se buscara consenso sobre el funcionamiento y operatividad, una vez obtenido dicho consenso deberá ser aprobada como exitosa la
tarea y llevada a cabo su migración definitiva.
Una vez que este servidor y el cliente estén completamente operativos y se cuente con el consenso, se realizara el clonado de ambas maquinas, las mismas serán destinadas
para atención al publico y ocho mas, para clientes que se encuentran en consultorios médicos. Existe un numero relativamente variable de notebooks de uso personal de los
médicos, que en primera instancia no serán migradas, debido al carácter personal de estas maquinas y a que sus dueños no desean que las mismas formen parte ni trabajar con
la red de la oficina, luego en etapa posterior y a requerimiento de los mismos se podrá hacer el cambio.
Por ultimo y debido al tipo de uso que se da a las computadoras en la clínica, (Software ofimático, Mensajería, Internet, e-mail, Impresiones y consulta, creación y
modificación de archivos en servidor), tareas relativamente transparentes con su contraparte del software privativo, se realizara una breve capacitación.

**Publicacion y Licenciamiento:**

Este trabajo será publicado en Gitlab bajo licencia Creative Commons CC By-SA.

**Documentación:**

La documentacion del proyecto se llevara acabo en este mismo proyecto de Gitlab.

https://gitlab.com/fabian_dn/Trabajo-Final.git
